﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using IdentityServer4.Models;
using System.Collections.Generic;

namespace IdentityServer
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> IdentityResources =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId()
            };

        public static IEnumerable<ApiScope> ApiScopes =>
            new ApiScope[]
            { };

        public static IEnumerable<Client> Clients =>
            new Client[]
            { };

        public static IEnumerable<ApiResource> GetApis()
        {
            return new List<ApiResource>
                    {
                        new ApiResource( "ApiName" )
                        {
                            ApiSecrets = {
                                new Secret( "secret_for_the_api".Sha256() )
                            }
                        }
                    };
        }

        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
    {
        new Client
        {
            ClientId = "ConsoleApp_ClientId",
            ClientSecrets = {
                new Secret( "secret_for_the_consoleapp".Sha256() )
            },
            //http://docs.identityserver.io/en/latest/topics/grant_types.html
            AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
            AllowedScopes = {
                "ApiName"
            },
        }
    };
        }
    }
}